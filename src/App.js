import React, { useState } from "react";
import Container from "@material-ui/core/Container";
import { makeStyles } from "@material-ui/core/styles";
import Menu from "./components/menu";
import Router from "./router";

const useStyles = makeStyles((_theme) => ({
  root: {
    display: "flex",
    justifyContent: "center",
    color: "#333",
  },
}));

function App() {
  const classes = useStyles();
  const [isAuthentication, setAuthentication] = useState(undefined);
  return (
    <React.Fragment>
      <Menu
        isAuthentication={isAuthentication}
        setAuthentication={setAuthentication}
      />
      <Container maxWidth="sm" className={classes.root}>
        <Router
          isAuthentication={isAuthentication}
          setAuthentication={setAuthentication}
        />
      </Container>
    </React.Fragment>
  );
}
export default App;
