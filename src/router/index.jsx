import { Switch, Route } from "react-router-dom";

import Login from "../pages/login";
import FormRegister from "../pages/form-register";
import Users from "../pages/users";
import UserFeedbacks from "../pages/user-feedbacks";
import FeedbacksForm from "../pages/feedbacks-form";

const Router = ({ setAuthentication, isAuthentication }) => {
  // if (isAuthentication === undefined) {
  //   return <h1>Loading...</h1>;
  // }

  if (!isAuthentication) {
    return (
      <Switch>
        <Route exact path="/register">
          <FormRegister />
        </Route>
        <Route exact path="/">
          <Login setAuthentication={setAuthentication} />
        </Route>
      </Switch>
    );
  }

  return (
    <Switch>
      <Route exact path="/users/feedbacks/:id/new">
        <FeedbacksForm />
      </Route>
      <Route exact path="/users/feedbacks/:id">
        <UserFeedbacks />
      </Route>
      <Route exact path="/users">
        <Users
          isAuthentication={isAuthentication}
          setAuthentication={setAuthentication}
        />
      </Route>
    </Switch>
  );
};

export default Router;
