import { useHistory, useParams } from "react-router-dom";
import { useForm } from "react-hook-form";
import axios from "axios";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

import { makeStyles } from "@material-ui/core/styles";
import { TextField, Button, Box } from "@material-ui/core/";

const useStyles = makeStyles((theme) => ({
  formLogin: {
    width: "100%",
    textAling: "center",
    "& > *": {
      margin: theme.spacing(1),
      width: "210px",
    },
  },
  boxForm: {
    width: "230px",
    margin: "10px",
    padding: "40px 20px 50px 20px",
    "& h1": {
      margin: "20px 8px",
      fontSize: "30px",
    },
  },
}));

const FeedbacksForm = () => {
  const schema = yup.object().shape({
    name: yup
      .string()
      .min(4, "Minimun 4 characters")
      .required("Name cannot be blank"),
    comment: yup
      .string()
      .min(6, "Minimun 6 characters")
      .required("Comment cannot be blank"),
    grade: yup
      .number()
      .typeError("Grade must be number")
      .lessThan(11, "Give a grade of 0 - 10")
      .moreThan(-1, "Give a grade of 0 - 10")
      .required("Grade cannot be blank"),
  });

  const { register, handleSubmit, errors, setError } = useForm({
    resolver: yupResolver(schema),
  });
  const params = useParams();
  const history = useHistory();

  const handleForm = (data) => {
    const idUser = params.id;

    axios
      .post(
        `https://ka-users-api.herokuapp.com/users/${idUser}/feedbacks`,
        {
          feedback: {
            name: data.name,
            comment: data.comment,
            grade: data.grade,
          },
        },
        {
          headers: { Authorization: window.localStorage.getItem("auth_token") },
        }
      )
      .then((res) => {
        history.push(`/users/feedbacks/${idUser}`);
      })
      .catch((err) => {
        setError("name", err.response.data.error);
      });
  };

  const classes = useStyles();
  return (
    <Box className={classes.boxForm}>
      <form className={classes.formLogin} onSubmit={handleSubmit(handleForm)}>
        <h1>New Feedbacks</h1>
        <div>
          <TextField
            color="primary"
            size="small"
            label="Name"
            variant="outlined"
            name="name"
            inputRef={register}
            error={!!errors.name}
            helperText={errors.name?.message}
          />
        </div>
        <div>
          <TextField
            color="primary"
            size="small"
            label="Comment"
            variant="outlined"
            name="comment"
            inputRef={register}
            error={!!errors.comment}
            helperText={errors.grade?.message}
          />
        </div>
        <div>
          <TextField
            color="primary"
            type="number"
            size="small"
            label="Grade"
            variant="outlined"
            name="grade"
            inputRef={register}
            error={!!errors.grade}
            helperText={errors.grade?.message}
          />
        </div>
        <div>
          <Button
            type="submit"
            variant="outlined"
            color="primary"
            fullWidth={true}
          >
            Send
          </Button>
        </div>
      </form>
    </Box>
  );
};

export default FeedbacksForm;
