import { useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import axios from "axios";

import { makeStyles } from "@material-ui/core/styles";
import { TextField, Button, Box } from "@material-ui/core/";

const useStyles = makeStyles((theme) => ({
  formLogin: {
    width: "100%",
    textAling: "center",
    "& > *": {
      margin: theme.spacing(1),
      width: "210px",
    },
  },
  boxForm: {
    width: "230px",
    margin: "10px",
    padding: "40px 20px 50px 20px",
    "& h1": {
      margin: "20px 8px",
    },
  },
}));

const Login = ({setAuthentication}) => {
  const history = useHistory();

  const schema = yup.object().shape({
    user: yup
      .string()
      .min(6, "Minimum 6 characters")
      .required("User cannot be blank"),
    password: yup
      .string()
      .min(6, "Minimum 6 characters")
      .matches(
        /^((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1}).*$/,
        "minimum 1 characters special"
      )
      .required("Password cannot be blank"),
  });

  const { register, handleSubmit, errors, setError } = useForm({
    resolver: yupResolver(schema),
  });

  const handleForm = (data) => {
    axios
      .post("https://ka-users-api.herokuapp.com/authenticate", {
        user: data.user,
        password: data.password,
      })
      .then((res) => {
        window.localStorage.setItem("auth_token", res.data.auth_token);
        setAuthentication(true);
        history.push("/users");
      })
      .catch((err) => {
        console.log(err);
        setError("password", {
          message: err.response.data.error.user_authentication,
        });
        setError("user", {
          message: err.response.data.error.user_authentication,
        });
      });
  };

  // Comentário pra eu fazer o commit

  const classes = useStyles();
  return (
    <Box className={classes.boxForm}>
      <form className={classes.formLogin} onSubmit={handleSubmit(handleForm)}>
        <h1>Login</h1>

        <div>
          <TextField
            color="primary"
            size="small"
            label="User"
            variant="outlined"
            name="user"
            inputRef={register}
            error={!!errors.user}
            helperText={errors.user?.message}
          />
        </div>
        <div>
          <TextField
            color="primary"
            size="small"
            label="Password"
            variant="outlined"
            name="password"
            inputRef={register}
            error={!!errors.password}
            helperText={errors.password?.message}
          />
        </div>
        <div>
          <Button
            fullWidth={true}
            variant="outlined"
            color="primary"
            type="submit"
          >
            Sign in
          </Button>
        </div>
      </form>
    </Box>
  );
};

export default Login;
