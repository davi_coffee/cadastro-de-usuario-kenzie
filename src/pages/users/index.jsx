import React, { useEffect, useState } from "react";
import axios from "axios";
import { Link, useHistory, useLocation } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";

const columns = [
  { id: "id", label: "Id", minWidth: 170 },
  { id: "name", label: "Name", minWidth: 100 },
  {
    id: "user",
    label: "User",
    minWidth: 170,
    align: "right",
  },
  {
    id: "email",
    label: "E-mail",
    minWidth: 170,
    align: "right",
  },
  {
    id: "feedbacks",
    label: "Feedbacks",
    minWidth: 170,
    align: "right",
  },
];

function createData(id, name, user, email, feedbacks) {
  return { id, name, user, email, feedbacks };
}

const useStyles = makeStyles({
  root: {
    width: "100%",
  },
  container: {
    maxHeight: 440,
  },
});

const StickyHeadTable = ({ setAuthentication, isAuthentication }) => {
  const history = useHistory();
  const { pathname } = useLocation();
  const [allUsers, setAllUsers] = useState(undefined);

  useEffect(() => {
    const token = window.localStorage.getItem("auth_token");

    if (!token) {
      setAuthentication(false);
    }

    axios
      .get("https://ka-users-api.herokuapp.com/users", {
        headers: {
          Authorization: token,
        },
      })
      .then((response) => {
        setAllUsers(
          response.data.map(({ id, name, user, email }) =>
            createData(
              id,
              name,
              user,
              email,
              <Link to={`/users/feedbacks/${id}`}>Go To Feedbacks</Link>
            )
          )
        );
        setAuthentication(true);
        if (pathname !== "/" && pathname !== "/register") {
          history.push(pathname);
        } else {
          history.push("/users");
        }
      })
      .catch(() => {
        setAuthentication(false);
        if (pathname === "/" || pathname === "/register") {
          history.push(pathname);
        } else {
          history.push("/");
        }
      });
  }, [history, setAuthentication, pathname]);

  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (_event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Paper className={classes.root}>
      <TableContainer className={classes.container}>
        <Table className={classes.table} stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {!!allUsers &&
              allUsers
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row) => {
                  return (
                    <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                      {columns.map((column) => {
                        const value = row[column.id];
                        return (
                          <TableCell key={column.id} align={column.align}>
                            {column.format && typeof value === "number"
                              ? column.format(value)
                              : value}
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={!!allUsers ? allUsers.length : 0}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Paper>
  );
};

export default StickyHeadTable;
