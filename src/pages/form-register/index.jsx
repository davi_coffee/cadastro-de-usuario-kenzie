import React from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import axios from "axios";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import { useHistory } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: "25ch",
    },
  },
}));

const Register = () => {
  const history = useHistory();
  const classes = useStyles();
  const schema = yup.object().shape({
    name: yup.string().required("Campo obrigatório."),
    user: yup
      .string()
      .min(6, "Deve conter pelo menos 6 letras.")
      .required("Campo obrigatório."),
    email: yup
      .string()
      .email("Digite um e-mail válido.")
      .required("Campo obrigatório."),
    password: yup
      .string()
      .min(6, "Deve conter pelo menos 6 letras.")
      .required("Campo obrigatório."),
    password_confirmation: yup
      .string()
      .oneOf([yup.ref("password")], "Senhas não coincidem."),
  });
  const { register, handleSubmit, errors, setError } = useForm({
    resolver: yupResolver(schema),
  });

  const sendNewUser = (data) => {
    axios
      .post("https://ka-users-api.herokuapp.com/users", { user: data })
      .then(() => history.push("/"))
      .catch((err) =>
        !!err.response.data.user[0] && !!err.response.data.email[0]
          ? (setError("user", { message: "User already exists" }),
            setError("email", { message: "E-mail already exists" }))
          : !!err.response.data.user[0]
          ? setError("user", { message: "User already exists" })
          : setError("email", { message: "E-mail already exists" })
      );
  };

  return (
    <div>
      <h1>Cadastro de usuário</h1>
      <form className={classes.root} onSubmit={handleSubmit(sendNewUser)}>
        <div>
          <TextField
            name="name"
            label="Nome"
            variant="outlined"
            inputRef={register}
            error={!!errors.name}
            helperText={errors.name?.message}
            size="small"
          />
        </div>
        <div>
          <TextField
            name="user"
            label="Usuário"
            variant="outlined"
            inputRef={register}
            error={!!errors.user}
            helperText={errors.user?.message}
            size="small"
          />
        </div>
        <div>
          <TextField
            name="email"
            label="E-mail"
            variant="outlined"
            inputRef={register}
            error={!!errors.email}
            helperText={errors.email?.message}
            size="small"
          />
        </div>
        <div>
          <TextField
            type="password"
            name="password"
            label="Senha"
            variant="outlined"
            inputRef={register}
            error={!!errors.password}
            helperText={errors.password?.message}
            size="small"
          />
        </div>
        <div>
          <TextField
            type="password"
            name="password_confirmation"
            label="Confirmar senha"
            variant="outlined"
            inputRef={register}
            error={!!errors.password_confirmation}
            helperText={errors.password_confirmation?.message}
            size="small"
          />
        </div>
        <div>
          <Button
            type="submit"
            variant="outlined"
            color="primary"
            fullWidth={true}
          >
            Enviar
          </Button>
        </div>
      </form>
    </div>
  );
};

export default Register;
