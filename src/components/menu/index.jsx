import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { useHistory } from "react-router-dom";

const a11yProps = (index) => {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
};

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

const Menu = ({ isAuthentication, setAuthentication }) => {
  const history = useHistory();
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const logout = () => {
    history.push("/");
    setAuthentication(false);
    window.localStorage.clear();
  };

  const handleChange = (_event, newValue) => {
    setValue(newValue);
    switch (newValue) {
      case 0:
        !isAuthentication ? history.push("/") : history.push("/users");
        break;
      case 1:
        !isAuthentication ? history.push("/register") : logout();
        break;
      default:
        break;
    }
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Tabs value={value} onChange={handleChange}>
          <Tab
            label={!isAuthentication ? "Login" : "Users"}
            {...a11yProps(0)}
          />
          <Tab
            label={!isAuthentication ? "New user" : "Logout"}
            {...a11yProps(1)}
          />
        </Tabs>
      </AppBar>
    </div>
  );
};

export default Menu;
